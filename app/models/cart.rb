class Cart < ApplicationRecord
  belongs_to :user
  has_many :cart_items, dependent: :destroy
  has_many :books, through: :cart_items

  # Kitap ekler veya var olan kitabın miktarını günceller
  def add_book(book, quantity)
    current_item = cart_items.find_by(book_id: book.id)
    if current_item
      current_item.quantity += quantity
      current_item.save
    else
      current_item = cart_items.create(book_id: book.id, quantity: quantity)
    end
    current_item
  end

  # Toplam sepet tutarını hesaplayan metod
  def total_amount
    cart_items.joins(:book).sum('books.price * cart_items.quantity')
  end

  def checkout
    clear_cart_items
    true
  end

  def clear_cart_items
    cart_items.destroy_all
  end

end
