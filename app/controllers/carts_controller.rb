class CartsController < ApplicationController
  before_action :authorize_request
  before_action :set_cart, only: [:show, :checkout, :clear]

  # Sepetin detaylarının görüntülenmesi
  def show
    render json: @cart.as_json(include: { cart_items: { include: :book } })
  end

  # Ödeme işleminin gerçekleştirilmesi
  def checkout
    if @cart.cart_items.any?
      total_amount = @cart.total_amount  #
      if process_payment(total_amount)  
        @cart.clear_cart_items  
        render json: {
          message: "Ödeme başarılı. Siparişiniz alındı. Toplam ödediğiniz tutar: #{total_amount} TL"}, status: :ok
      else
        render json: {
          message: 'Ödeme işlemi başarısız. Lütfen tekrar deneyiniz.'
        }, status: :unprocessable_entity
      end
    else
      render json: { message: 'Sepetiniz boş.' }, status: :unprocessable_entity
    end
  end

  
  def clear
    @cart.clear
    render json: { message: 'Sepet temizlendi.' }, status: :ok
  end
  private


  # Ödeme işlemini simüle eden metod
  def process_payment(amount)

    true
  end

  # Kullanıcının sepetini ayarlayan yardımcı metod
  def set_cart
    @cart = current_user.cart || current_user.create_cart
  end
end
